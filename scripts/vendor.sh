#!/bin/bash

run_vendor()
{

local LOCALDIR="${2-$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)}"
local BASE="${LOCALDIR%scripts}" # input

rm -rf "$BASE"/output/{de,en,eo,fr}/vendor/

echo "$BASE"output/{de,en,eo,fr} | xargs -n 1 cp -rf "$BASE"/output/vendor

}

# ==============================================================================
# EXECUTION - START
# ==============================================================================
run_vendor "$@"
