Author: Megver83
Category: GNU/Linux
Date: 2017-05-18 06:35
Image: /2017/05/xmpp.png
Slug: conectar-xmpp-con-whatsapp-usando-yowsup-y-transwhat
Tags: xmpp
Title: Conectar XMPP con WhatsApp usando yowsup y transWhat

¿No te ha pasado que la comunicación con tus amigos, familia, etc.
se dificulta por el simple hecho de no usar WhatsApp? Claro, es una
solución rápida, fácil y además multiplataforma, pero no respeta las
libertades y derechos de los usuarios. Conozco mucha gente del mundo
del software libre que se ha visto obligada a instalarlo porque no hay
otra forma de comunicarse con quienes necesitan hacerlo (sea por trabajo,
tareas escolares, o simplemente mantener contacto con sus seres queridos).
Sin embargo, con [yowsup][yowsupGIT] y [transWhat][transWhatGIT]
eso se acabó. Con ambos, podrás
chatear con los usuarios de WhatsApp, estar en sus grupos y más,
utilizando la red de mensajería instantánea descentralizada más conocida: XMPP.

## ¿Qué son yowsup y transWhat?

Yowsup es una biblioteca de python que te permite crear aplicaciones que usan
el servicio de WhatsApp. Yowsup se ha utilizado para crear un cliente no
oficial de WhatsApp Nokia N9 a través del proyecto [Wazapp][WazappGIT]
que estaba en uso por 200K + usuarios, así como otro completamente oficioso cliente
no oficial para Blackberry 10. Por el otro lado, transWhat es una puerta de
enlace entre las redes de mensajería instantánea XMPP y WhatsApp.

## Genial! ¿Cómo empiezo?

Los requisitos básicos son:

- Tener [pip][pipweb] instalado.
- Crearse una cuenta (en el caso de no tenerla) de XMPP con soporte para la pasarela transWhat. Por ejemplo, [JabJab.de][jabjabsite]
- Tener un número de teléfono móvil (celular).
- Tener un cliente XMPP avanzado, por ejemplo Gajim, con la cuenta XMPP a usarse configurada.

## Entonces, ¡Manos a la obra!

Desde un terminal, instala el paquete yowsup2 con pip como root.

    # pip install yowsup2

Después de haberlo instalado, asegúrate que en el archivo
/usr/lib/python3.6/site-packages/yowsup/env/env.py
en la variable DEFAULT diga "Android" (incluyendo las comillas),
lee [este comentario][comment]
de un issue del repositorio GitHub de yowsup.


## Creación de la cuenta

Para crearte una cuenta con yowsup, debes ejecutar el siguiente comando:

```bash
$ yowsup-cli registration -E s40 -r sms -p <nº_de_teléfono> -C <código_del_país> -m <código_móvil_del_país> -n <código_móvil_de_la_red>
```

Entonces, como dijo Jack el destripador, vamos por parte.


- `<nº_de_teléfono>` debe ser el número de teléfono completo, con el código del país, exceptuando el signo +
- `<código_del_país>` corresponde al código del país, si no te sabes el tuyo, míralo [aquí][netlist].
- `<código_móvil_del_país>` Es el MCC que lo puedes encontrar [aquí][MCC].
- `<código_móvil_de_la_red>` Es el MNC que también lo puedes encontrar en la misma página que `<código_móvil_del_país>`

Si todo funcionó, debería aparecerte algo como esto al final del comando, deberías recibir
un mensaje de texto con el código para registrarte, de forma similar, escribe:

```bash
$ yowsup-cli registration -E s40 -p <nº_de_teléfono> -C <código_del_país> -m <código_móvil_del_país> -n <código_móvil_de_la_red> -R <código_del_sms>
```

Donde `<código_del_sms>` corresponde al código que recibiste en tu celular.
El resultado del comando, cerca del final, deberías obtener algo como:

```bash
INFO:yowsup.common.http.warequest:b'{"status":"ok","login":"<nº_de_teléfono>","type":"existing","pw":"FBmvgZs8UUbSX2ZHeVyxc7G7g4s=","expiration":4444444444.0,"kind":"free","price":"US$0.99","cost":"0.99","currency":"USD","price_expiration":1497967560}\n' status: b'ok' login: b'<nº_de_teléfono>' pw: b'<contraseña>' type: b'existing' expiration: 4444444444.0 kind: b'free' price: b'US$0.99' cost: b'0.99' currency: b'USD' price_expiration: 1497967560
```

Lo que nos interesa de aquí es lo que está ennegrecido. A ustedes les mostrará su número de teléfono y una contraseña,
que ahora usaremos para conectar XMPP con WhatsApp. Nota que las letras “b” y los apostrofes no están ennegrecidos.

## Conectando XMPP y WhatsApp con transWhat

Entonces, ahora que tenemos el número y la contraseña, es hora de la acción. Si usas Gajim, ve a **Acciones> Descubrir servicios>
usando la cuenta jabjab.de** (o la que ustedes usen con soporte transWhat). En la sección "Transportes" selecciona "transWhat" y
luego haz click en "Suscribir", te pedirá el número y contraseña que obtuviste con yowsup.

Listo! Ya tienes una cuenta XMPP funcional con WhatsApp. Para agregar contactos de WhatsApp tienes que
añadirlos con la dirección `<nº_de_teléfono>@dominio.del.transporte>`

**Nota:**

`<nº_de_teléfono>`: tiene que ser el número completo pero sin el **signo +**, igual que cuando te registraste.

## Para finalizar

Esperamos que te haya servido este tutorial, compártelo con tus amigos, en redes sociales y has derivados de este artículo si lo deseas,
recuerda que Conocimientos Libres se trata de eso (o˘◡˘o)

Le agradezco a [trinux][u-trinux], quien fue la persona que me enseñó como conectar XMPP y WhatsApp.

[yowsupGIT]: https://github.com/tgalal/yowsup
[transWhatGIT]: https://github.com/stv0g/transwhat
[WazappGIT]: https://github.com/tgalal/wazapp
[pipweb]: https://pip.pypa.io/
[jabjabsite]: https://jabjab.de/
[comment]: https://github.com/tgalal/yowsup/issues/1952#issuecomment-284212268
[netlist]: http://www.ipipi.com/networkList.do
[MCC]: https://en.wikipedia.org/wiki/Mobile_country_code
[u-trinux]: https://trisquel.info/es/users/trinux
