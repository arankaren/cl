Author: Jorge Maldonado Ventura
Category: Cine
Date: 2017-02-01 00:03
Image: 2017/02/article-locutus.png
Lang: es
Modified: 2019-02-10 12:23
Slug: locutus-de-gnu
Status: published
Tags: GNU/Linux, gracioso, humor, software libre, software privativo, Star Trek, video
Title: Locutus de GNU

Encontré [esta parodia](https://goblinrefuge.com/mediagoblin/u/locutus/m/locutus-de-gnu/)
muy graciosa. Para entenderla al menos debéis saber qué son el software
libre y el software privativo. Espero que os guste.

<video playsinline controls
       poster="{static}/wp-content/uploads/article/images/2017/02/locutus.png">
  <source src="https://archive.org/download/libreweb/locutus.webm" type="video/webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>
