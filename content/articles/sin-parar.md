Author: Jorge Maldonado Ventura
Category: Cine
Date: 2017-07-16 12:15
Image: 2017/07/intro-articulo-sin-parar.png
Lang: es
Modified: 2019-02-10 11:46:44
Slug: sin-parar
Tags: capitalismo, ciencia, cortometraje, cultura libre, educación, sistema, video
Title: Sin parar

«Desde que hace siglos apareciera el mayor invento creado por el hombre,
generación tras generación hay alguien encargado de revisarlo
minuciosamente [...] y que pueda seguir funcionando en todo el mundo sin
parar». Así comienza este cortometraje educativo sobre el sistema
capitalista. ¿Seguirá funcionando siempre **sin parar**?

<video playsinline controls poster="{static}/wp-content/uploads/article/images/2017/07/poster-video-sin-parar.png">
  <source src="https://archive.org/download/libreweb/sin-parar.webm" type="video/webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

El vídeo se encuentra bajo la licencia <a href="http://creativecommons.org/licenses/by-sa/3.0/"><abbr title="Attribution-ShareAlike 3.0 Unported">CC BY-SA 3.0</abbr></a> y fue
obtenido de
[la instancia de Mediagoblin de Roaming Initiative](http://roaming-initiative.com/mediagoblin/u/rafapoverello/m/sin-parar/).
