Author: Jesús E.
Category: GNU/Linux
Date: 2018-05-29 08:57
Image: 2018/05/curso-hyperbola-gnu-linux.png
Slug: curso-de-instalacion-de-hyperbola
Tags: hyperbola, linux, linux-libre, parabola
Title: Curso de Instalación de Hyperbola

Después de algunos meses, he decidido realizar esta serie de vídeos
sobre la instalación de [Hyperbola GNU+Linux-Libre][hypersite],
una de las distros 100 % libres, el método de Instalación se basa
en una guía de instalación, bien abajo se encuentra una lista
de 6 vídeos:

<video id="videoplaylist" playsinline controls poster='{static}/wp-content/uploads/article/images/2018/05/curso-hyperbola-1.png'>
    <source src="https://archive.org/download/hyperbola-video-1/Hyperbola%20Base%20%2B%20Usuario%201%E2%81%846.webm" type="video/webm"/>
    <p>Lo siento, tu navegador no soporta vídeo en HTML5. Por favor, cambia o actualiza tu navegador web</p>
</video>

## Lista de vídeos

<aside class="menu">
    <ul id="playlist" class="menu-list play-menu">
        <li class="is-active-play">
            <a href="https://archive.org/download/hyperbola-video-1/Hyperbola%20Base%20%2B%20Usuario%201%E2%81%846.webm">
                Hyperbola [Base + Usuario] 1⁄6 | Time: 1:06:45
            </a>
        </li>
        <li>
            <a href="https://archive.org/download/hyperbola-video-2/Hyperbola%20Xorg%202%E2%81%846.webm">
                Hyperbola [Xorg] 2⁄6 | Time: 23:38
            </a>
        </li>
        <li>
            <a href="https://archive.org/download/hyperbola_20180527/Hyperbola%20XFCE%203%E2%81%846.webm">
                Hyperbola [XFCE] 3⁄6 | Time: 20:06
            </a>
        </li>
        <li>
            <a href="https://archive.org/download/hyperbola_20180527_2333/Hyperbola%20Fixed%20Idioma%204%E2%81%84%E2%81%846.webm">
                Hyperbola [Fixed Idioma] 4⁄6 | Time: 6:31
            </a>
        </li>
        <li>
            <a href="https://archive.org/download/hyperbola-video-5/Hyperbola%20Gestor%20de%20Inicio%20de%20Sesi%C3%B3n%20SLIM%205%E2%81%846.webm">
                Hyperbola [Gestor de Inicio de Sesión SLIM] 5⁄6 | Time: 7:23
            </a>
        </li>
        <li>
            <a href="https://archive.org/download/hyperbola-video-6/Hyperbola%20-%20Utilidades%206%E2%81%846.webm">
                Hyperbola [Utilidades] 6⁄6 | Time: 34:14
            </a>
        </li>
    </ul>
</aside>

>Y, bien hasta aquí ya es posible instalar Hyperbola GNU+Linux-Libre, de manera sencilla.

<!-- playlist -->
<script src="{static}/vendor/aplaylist/videoplaylist.js"></script>
<!-- /playlist -->

[hypersite]: https://www.hyperbola.info/
