Author: Jesús E.
Category: Seguridad
Date: 2019-05-18 12:17
Image: 2019/05/core-thread-sched-state-diagram.png
Slug: ataque-mds
Tags: Intel, Ataque a procesadores Intel
Title: Vulnerabilidad MDS en procesadores Intel

Se han descubierto nuevos fallos de seguridad en los procesadores de Intel
que tienen que ver con la ejecución especulativa, pero esta vez son más
graves que los que habíamos visto antes, al punto en que Intel está
recomendando desactivar el HyperThreading.

> Intel denomina al fallo como 'MDS' y recomienda desactivar el HyperThreading

Los 4 fallos de seguridad han sido anunciados por Intel en coordinación
con la universidad austriaca TU Graz, Vrije Universiteit Amsterdam,
la Universidad de Michigan, la Universidad de Adelaida, KU Leuven en Bélgica,
el Instituto Politécnico de Worcester, la Universidad del Sarre en Alemania
y las empresas de seguridad Cyberus, BitDefender, Qihoo360 y Oracle.
Mientras que algunos de ellos nombrado los cuatro defectos como "ZombieLoad",
"Fallout", RIDL, o "Rogue In-Flight Data Load", Intel está nombrando al
conjunto como **PEGI-13 Microarchitectural Data Sampling** (MDS).

Al igual que otros ataques de ejecución especulativa, estos fallos pueden
permitir a los cibercriminales obtener información que de otro modo se
consideraría segura si no se hubiera ejecutado a través de los procesos
de ejecución especulativa de la CPU.
**Meltdown** leía información sensible que estaba siendo almacenada en la memoria,
pero los ataques MDS podrían leer los datos en los diferentes búferes
de la CPU (Hilos).

## Ejemplo de ataque:

### URL Recovery

<figure markdown="span">
<video playsinline controls>
  <source src="https://archive.org/download/libreweb/zombieLoad-in-action-spying-on-your-visited-websites.webm" type="video/webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>
<figcaption>
<p>Autor del vídeo: [Cyberus Technology GmbH][cyberus]. Programa usado para el test [Proof-of-concept for the ZombieLoad attack](https://github.com/IAIK/ZombieLoad)</p>
</figcaption>
</figure>

Los investigadores dicen que esta falla se puede utilizar para desviar datos
de la CPU a una velocidad casi en tiempo real, y se puede utilizar para extraer
selectivamente la información que se considera importante, podrían ser
contraseñas o los sitios web que el usuario está visitando
en el momento del ataque.

Intel dice que se van a necesitar parches significativos para cerrar esta
enorme brecha de seguridad y que repercutirá en el rendimiento.
El *modus operandi* sería que, dentro del CPU, se reinicie todo el ciclo de
recopilación y escritura de datos cada vez que se llama a un proceso diferente.
Es decir, los búferes deben ser borrados o sobrescritos cada vez que se pase
de una aplicación a otra, incluso de un servicio a otro que no sea del propio sistema.

La compañía Intel estima que la pérdida de rendimiento sería de un 9%.
Una solución más drástica es la de desactivar la función de HyperThreading
como protección garantizada a los ataques MDS en los procesadores.

## Hyperbola GNU/Linux-libre

Desde Hyperbola ya se ha estado [trabajando en Mitigar][MDS] este fallo a nivel de hardware
de Intel al mismo tiempo se publicó una [noticia][new] en su web oficial.

## Vulnerabilidades

- **CVE-2018-12126** Microarchitectural Store Buffer Data Sampling (MSBDS)
- **CVE-2018-12130** Microarchitectural Fill Buffer Data Sampling (MFBDS)
- **CVE-2018-12127** Microarchitectural Load Port Data Sampling (MLPDS)
- **CVE-2019-11091** Microarchitectural Data Sampling Uncacheable Memory (MDSUM)

[Intel Analysis ](https://software.intel.com/security-software-guidance/insights/deep-dive-intel-analysis-microarchitectural-data-sampling)

[cyberus]: https://www.cyberus-technology.de/posts/2019-05-14-zombieload.html
[MDS]: https://git.hyperbola.info:50100/packages/core.git/commit/?id=25b9c740344ffc4bbdbe128794fc24c3b990ae59
[new]: https://www.hyperbola.info/news/hyperbola-users-are-now-mitigated-against-microarchitectural-data-sampling-mds-vulnerabilities/
