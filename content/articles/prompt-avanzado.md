Author: Jesús E.
Category: GNU/Linux
Date: 2017-12-05 11:34
Modified: 2019-02-06 11:34
Image: 2017/12/bash.png
Slug: prompt-avanzado
Tags: bash, hyperbash, shell
Title: Prompt avanzado

Muchas veces necesitamos del [intérprete de comandos][bash]
para realizar una que otra tarea, quizás resulte tedioso armar
nuestra propia configuración de `.bashrc`.
Pero como en la [World Wide Web][www] existe mucha información
útil, se ha logrado escribir una configuración prudente del ya
mencionado `.bashrc` para distros basadas en Arch como
[Hyperbola][hypersite] o [Parabola][parasite], en efecto estas
2 últimas son distros 100 % Libres.

¿Y dónde consigo una copia?, sencillo puedes descargarlo desde
[notabug][notabug] bajo la Licencia [GPLv3][license].

¿Existe algún vídeo, mostrando su uso? sí, a continuación te
mostramos cómo instalarlo:

<video playsinline controls>
  <source src="https://archive.org/download/libreweb/0001-15599.webm" type="video/webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

[bash]: https://es.wikipedia.org/wiki/Bash
[www]: https://es.wikipedia.org/wiki/World_Wide_Web
[hypersite]: https://hyperbola.info/
[parasite]: https://parabola.nu/
[notabug]: https://notabug.org/heckyel/hyperbash
[license]: https://www.gnu.org/licenses/gpl-3.0.html
