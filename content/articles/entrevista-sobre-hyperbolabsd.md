Author: Jesús E.
Category: Hyperbola
Date: 2020-01-17 07:32
Image: 2020/01/hyperbola-bsd.jpg
Lang: es
Slug: entrevista-sobre-hyperbolabsd
Tags: HyperbolaBSD, Entrevista
Title: Entrevista sobre HyperbolaBSD

A fines de diciembre de 2019, Hyperbola anunció que realizarían
cambios importantes en su proyecto. Han decidido abandonar el
kernel Linux a favor de bifurcar el kernel de OpenBSD.
Este anuncio solo llegó meses después de que Project Trident
anunciara que iban en la dirección opuesta (de BSD a GNU/Linux).

Hyperbola también planea reemplazar todo el software que no sea
compatible con GPLv3 con las nuevas versiones que sí lo son.

Para obtener más información sobre el futuro de su nuevo proyecto,
entrevisté a André, cofundador de Hyperbola.

### ¿Por qué Hyperbola GNU/Linux-libre se convirtirá en HyperbolaBSD?

**It's FOSS,**

###### En su anuncio, declara que el kernel Linux "avanza rápidamente por un camino inestable". ¿Podría explicar qué quiere decir con eso?

**André Silva,**

>En primer lugar, incluye la adaptación de funciones
>DRM como [HDCP](https://patchwork.kernel.org/patch/10084131/)
>(Protección de contenido digital de alto ancho de banda).
>Actualmente hay una opción para deshabilitarlo en el momento de la
>compilación, sin embargo, no existe una política que nos garantice
>que será opcional para siempre.

<!--- -->

>Históricamente, algunas características comenzaron como opcionales
>hasta que alcanzaron la funcionalidad total. Luego se volvieron
>forzados y difíciles de arreglar. Incluso si esto no sucede en el
>caso de HDCP, seguimos siendo cautelosos acerca de tales
>implementaciones.

<!--- -->

>Otra de las razones es que el kernel Linux ya no se está endureciendo
>adecuadamente. [Grsecurity](https://grsecurity.net/) dejó de ofrecer
>parches públicos hace varios años, y dependíamos de eso para la
>seguridad de nuestro sistema. Aún si podríamos usar sus parches para
>una suscripción muy costosa, la suscripción finalizaría si
>decidiéramos hacer públicos esos parches.

<!--- -->

>Dichas restricciones van en contra de los principios de FSDG que
>requieren que proporcionemos código fuente completo, desbloqueado y
>sin restricciones a nuestros usuarios.

<!--- -->

>[KSPP](https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project)
>es un proyecto que tenía la intención de subir Grsec al kernel,
>pero hasta ahora no ha llegado a alcanzar el nivel de endurecimiento
>del kernel de Grsec/PaX. Tampoco ha habido muchos desarrollos recientes,
>lo que nos lleva a creer que ahora es un proyecto inactivo en su mayor
>parte.

<!--- -->

>Por último, el interés en [permitir que los módulos de Rust](https://lwn.net/Articles/797828/)
>ingresen al kernel es un problema para nosotros, debido a las
>restricciones de marcas registradas de Rust que nos impiden aplicar
>parches en nuestra distribución sin permiso expreso. Aplicamos
>parches para eliminar software no libre, archivos sin licencia y
>mejoras a la privacidad del usuario donde sea aplicable. También
>esperamos que nuestros usuarios puedan reutilizar nuestro código
>sin restricciones o permisos adicionales requeridos.

<!--- -->

>Esto también se debe en parte a que utilizamos UXP, un motor de
>navegador totalmente libre y un kit de herramientas de aplicación
>sin Rust, para nuestras aplicaciones de correo y navegador.

<!--- -->

>Debido a estas restricciones, y la preocupación de que en algún
>momento se convierta en una dependencia forzada del tiempo de
>compilación del kernel, necesitábamos otra opción.

**It's FOSS,**

###### También dijo en el anuncio que estaría bifurcando el kernel de OpenBSD. ¿Por qué elegiste OpenBSD sobre FreeBSD, el kernel DragonflyBSD o el kernel MidnightBSD?

**André Silva,**

>[OpenBSD](https://www.openbsd.org/) fue elegido como
>nuestra base para el hard-forking porque es un sistema que
>siempre ha tenido en cuenta el código de calidad y la seguridad.

<!--- -->

>Algunas de sus ideas que nos interesaron enormemente fueron las
>nuevas llamadas al sistema, incluidas pledge y unveil, que agrega
>un endurecimiento adicional al espacio del usuario y la eliminación
>de la herramienta de aplicación de políticas del sistema systrace.

<!--- -->

>También son conocidos por [Xenocara](https://www.xenocara.org/)
>y [LibreSSL](https://www.libressl.org/), los cuales ya
>habíamos estado usando después de portarlos a GNU/Linux-libre.
>Los encontramos bien escritos y generalmente más estables que
>Xorg/OpenSSL respectivamente.

<!--- -->

>Ninguna de las otras implementaciones de BSD que conocemos tiene
>ese nivel de seguridad. También sabíamos que [LibertyBSD](https://libertybsd.net/)
>ha estado trabajando para liberar el kernel de OpenBSD, lo
>que nos permitió usar sus parches para comenzar el
>desarrollo inicial.

**It's FOSS,**

###### ¿Por qué bifurcar el kernel en primer lugar? ¿Cómo mantendrá actualizado el nuevo kernel con soporte de hardware más nuevo?

**André Silva,**

>El kernel es una de las partes más importantes de
>cualquier sistema operativo, y consideramos que es fundamental
>comenzar con una base firme para avanzar.

<!--- -->

>Para la primera versión, planeamos mantenernos sincronizados
>con OpenBSD donde sea posible. En futuras versiones, podemos adaptar
>el código de otros BSD e incluso el kernel Linux donde sea necesario
>para mantenernos al día con el soporte y las características
>del hardware.

<!--- -->

>Estamos trabajando en coordinación con [Libreware Group](https://en.libreware.info/)
>(nuestro representante para actividades comerciales) y
>tenemos planes de abrir nuestra fundación pronto.

<!--- -->

>Esto ayudará a mantener el desarrollo, contratar futuros desarrolladores
>y alentar a los nuevos entusiastas a obtener soporte y código de hardware
>más nuevos. Sabemos que el desbloqueo no es suficiente porque es
>una mitigación, no una solución para nosotros. Entonces, por esa
>razón, necesitamos mejorar nuestra estructura y pasar a la siguiente
>etapa de desarrollo de nuestros proyectos.

**It's FOSS,**

###### Usted declara que planea reemplazar las partes del kernel de OpenBSD y el espacio de usuario que no son compatibles con GPL o que no son libres con los que sí lo son. ¿Qué porcentaje del código cae en la zona no GPL?

**André Silva,**

>Es alrededor del 20% en el núcleo de OpenBSD y el espacio de usuario.

<!--- -->

>En su mayoría, las partes con licencia no compatibles con GPL están bajo la
>licencia BSD original, a veces llamada la "licencia BSD de 4 cláusulas"
>que contiene una falla grave: la cláusula "obnoxious BSD advertising".
>No es fatal, pero nos causa problemas prácticos porque genera
>incompatibilidad con nuestro código y desarrollo futuro bajo GPLv3 y LGPLv3.

<!--- -->

>Los archivos no libres en OpenBSD incluyen archivos sin un encabezado
>de licencia apropiado, o sin una licencia en la carpeta que contiene
>un componente en particular.

<!--- -->

>Si esos archivos no contienen una licencia para dar a los usuarios las
>cuatro libertades esenciales o si no se ha agregado explícitamente en el
>dominio público, no es software libre. Algunos desarrolladores piensan
>que el código sin licencia está automáticamente en el dominio público.
>Eso no es cierto según la ley de derechos de autor de hoy; más bien,
>todos los trabajos con derechos de autor tienen derechos de autor
>por defecto.

<!--- -->

>Los blobs de firmware no libres en OpenBSD incluyen varios firmwares
>de hardware. Estos blobs de firmware también se producen en el kernel
>de Linux y han sido eliminados manualmente por el Linux-libre project
>durante años después de cada nueva versión del kernel.

<!--- -->

>Por lo general, tienen la forma de un binario codificado hexadecimal
>y se proporcionan a los desarrolladores del kernel sin fuente para
>proporcionar soporte para hardware de diseño exclusivo. Estos blobs
>pueden contener vulnerabilidades o puertas traseras además de violar
>su libertad, pero nadie lo sabría ya que el código fuente no está
>disponible para ellos. Deben eliminarse para respetar la
>libertad del usuario.

**It's FOSS,**

###### Estaba hablando con alguien sobre HyperbolaBSD y mencionaron [HardenedBSD](https://hardenedbsd.org/). ¿Has considerado HardenedBSD?

**André Silva,**

>Hemos investigado HardenedBSD, pero fue bifurcado de FreeBSD.
>FreeBSD tiene una base de código mucho más grande. Si bien
>HardenedBSD es probablemente un buen proyecto, requeriría mucho
>más esfuerzo para desbloquear y verificar las licencias de
>todos los archivos.

<!--- -->

>Decidimos usar OpenBSD como base para bifurcar en lugar de
>FreeBSD debido a su compromiso anterior con la calidad
>del código, la seguridad y el minimalismo.

**It's FOSS,**

###### Usted mencionó UXP (o [plataforma XUL unificada](http://thereisonlyxul.org/)). Parece que está utilizando el [fork de Moonchild de la base de código pre-Servo Mozilla](https://github.com/MoonchildProductions/UXP) para escribir un conjunto de aplicaciones para la web. ¿Es por el tamaño de la misma?

**André Silva,**

>Sí. Nuestra decisión de usar UXP fue por varias razones.
>Ya estuvimos renombrando Firefox como Iceweasel durante varios
>años para eliminar DRM, deshabilitar la telemetría y aplicar
>opciones de privacidad preestablecidas. Sin embargo, se hizo cada
>vez más difícil para nosotros mantenerlo cuando Mozilla seguía
>agregando anti-funciones, eliminando la personalización del usuario
>y rompiendo rápidamente nuestros parches de cambio de marca y privacidad.

<!--- -->

>Después de FF52, todas las extensiones XUL se eliminaron a favor
>de WebExt y Rust se hizo dependencia en el momento de la compilación.
>Mantenemos varios complementos XUL para mejorar la privacidad/seguridad
>del usuario que ya no funcionaría en el nuevo motor.
>También nos preocupaba que los complementos limitados de WebExt
>presentaran problemas de privacidad adicionales. Por ejemplo:
>cada complemento WebExt instalado contiene un UUID que se puede
>utilizar para identificar de forma única y precisa a los usuarios
>(consulte Bugzilla [1372288](https://bugzilla.mozilla.org/show_bug.cgi?id=1372288)).

<!--- -->

>Después de un poco de investigación, descubrimos UXP y que
>regularmente se mantiene al día con las correcciones de seguridad
>sin apresurarse a implementar nuevas funciones. Ya habían
>deshabilitado la telemetría en el kit de herramientas y siguen
>comprometidos a eliminarlo de la base de código.

<!--- -->

>Sabíamos que esto estaba bien alineado con nuestros objetivos,
>pero aún necesitábamos aplicar algunos parches para ajustar la
>configuración de privacidad y eliminar DRM. Por lo tanto,
>comenzamos a escribir nuestras propias aplicaciones en la parte
>superior del kit de herramientas.

<!--- -->

>Esto nos ha permitido ir mucho más allá del rebranding/deblobbing
>básico como lo hacíamos antes y escribir nuestras propias aplicaciones
>XUL totalmente personalizadas. Actualmente mantenemos
>[Iceweasel-UXP](https://wiki.hyperbola.info/doku.php?id=en:project:iceweasel-uxp),
>[Icedove-UXP](https://wiki.hyperbola.info/doku.php?id=en:project:icedove-uxp)
>e [Iceape-UXP](https://wiki.hyperbola.info/doku.php?id=en:project:iceape-uxp),
>además de compartir las mejoras del kit de herramientas de regreso a UXP.

**It's FOSS,**

###### En una [publicación del foro](https://forums.hyperbola.info/viewtopic.php?id=315), noté menciones de HyperRC, HyperBLibC e hyperman. ¿Son estos forks o reescrituras de herramientas BSD actuales compatibles con GPL?

**André Silva,**

> Son forks de proyectos existentes.

<!--- -->

>Hyperman es un fork de nuestro actual administrador de paquetes, pacman.
>Como pacman no funciona actualmente en BSD, y el soporte mínimo que tenía
>en el pasado se eliminó en versiones recientes, se requería un fork.
>Hyperman ya tiene una implementación funcional con el soporte de LibreSSL y BSD.

<!--- -->

>HyperRC será una versión parcheada de OpenRC init. HyperBLibC será un fork de BSD LibC.

**It's FOSS,**

###### Desde el principio de los tiempos, Linux ha defendido la licencia GPL y BSD ha defendido la licencia BSD. Ahora, está trabajando para escribir un BSD con licencia GPL. ¿Cómo respondería a aquellos en la comunidad BSD que no están de acuerdo con este movimiento?

**André Silva,**

>Somos conscientes de que hay desacuerdos entre el mundo
>GPL y BSD. Incluso hay desacuerdos sobre llamar a nuestra distribución
>anterior "GNU/Linux" en lugar de simplemente "Linux", ya que la última
>definición ignora que el espacio de usuario de GNU fue escrito en 1984,
>varios años antes de que Linus Torvalds escribiera el kernel Linux.
>Fueron los dos software diferentes combinados los que formaron un
>sistema completo.

<!--- -->

>Algunas de las principales diferencias con respecto a BSD,
>es que la GPL requiere que nuestro código fuente se haga público,
>incluidas las versiones futuras, y que solo se pueda usar junto
>con archivos con licencia compatible. Los sistemas BSD no tienen
>que compartir su código fuente públicamente, y pueden agruparse
>con varias licencias y software no libre sin restricciones.

<!--- -->

>Dado que apoyamos firmemente el Movimiento de Software Libre y
>deseamos que nuestro código futuro permanezca siempre en el
>espacio público, elegimos la GPL.

**It's FOSS,**

###### Sé que en este momento recién está comenzando el proceso, pero ¿tiene alguna idea de quién podría tener una versión utilizable de HyperbolaBSD disponible?

**André Silva,**

>Esperamos tener una versión alfa lista para 2021 (Q3)
>para la prueba inicial.

**It's FOSS,**

###### ¿Cuánto tiempo continuará admitiendo la versión actual de Hyperbola para Linux? ¿Será fácil para los usuarios actuales cambiar?

**André Silva,**

>Según nuestro anuncio, continuaremos admitiendo
>Hyperbola GNU/Linux-libre hasta 2022 (Q4). Esperamos que haya
>alguna dificultad en la migración debido a los cambios de ABI,
>pero prepararemos un anuncio e información en nuestro wiki una
>vez que esté listo.

**It's FOSS,**

###### Si alguien está interesado en ayudarlo a trabajar en HyperbolaBSD, ¿cómo puede hacerlo? ¿Qué tipo de experiencia estarías buscando?

**André Silva,**

>Cualquiera que esté interesado y pueda aprender es
>bienvenido. Necesitamos programadores y usuarios de C que
>estén interesados en mejorar la seguridad y la privacidad del
>software. Los desarrolladores deben seguir los principios FSDG
>de desarrollo de software libre, así como el principio YAGNI,
>lo que significa que implementaremos nuevas funciones solo
>cuando las necesitemos.

<!--- -->

>Los usuarios pueden bifurcar nuestro repositorio git y
>enviarnos parches para su inclusión.

**It's FOSS,**

###### ¿Tienes algún plan para soportar ZFS? ¿Qué sistemas de archivos admitirán?

**André Silva,**

>El soporte de [ZFS](https://itsfoss.com/what-is-zfs/)
>no está planeado actualmente, porque usa la Licencia de
>Desarrollo y Distribución Común, versión 1.0 (CDDL).
>Esta licencia es incompatible con todas las versiones
>de la GNU General Public License (GPL).

<!--- -->

>Sería posible escribir un nuevo código bajo GPLv3 y
>liberarlo con un nuevo nombre (por ejemplo, HyperZFS),
>sin embargo, no hay una decisión oficial de incluir el
>código de compatibilidad ZFS en HyperbolaBSD en este momento.

<!--- -->

>Tenemos planes de portar BTRFS, JFS2, CHFS de NetBSD, HAMMER/HAMMER2
>de DragonFlyBSD y JFFS2 del kernel Linux, todos los cuales tienen
>licencias compatibles con GPLv3. A largo plazo, también podemos
>admitir Ext4, F2FS, ReiserFS y Reiser4, pero deberán reescribirse
>debido a que tienen licencia exclusiva bajo GPLv2, que no permite
>su uso con GPLv3. Todos estos sistemas de archivos requerirán
>pruebas de desarrollo y estabilidad, por lo que estarán en versiones
>posteriores de HyperbolaBSD y no para nuestras versiones
>estables iniciales.

Me gustaría agradecer a [André Silva](https://www.hyperbola.info/members/founders/#Emulatorman)
por tomarse el tiempo para responder mis preguntas y por revelar
más sobre el futuro de HyperbolaBSD.

¿Qué piensa usted sobre Hyperbola para cambiar a un kernel BSD?
¿Qué opinas sobre un BSD lanzado bajo la GPL? Por favor háznoslo
saber en los comentarios más abajo.

Fuente: [https://itsfoss.com/hyperbola-linux-bsd/](https://itsfoss.com/hyperbola-linux-bsd/)
