Author: Jesús E.
Category: Cine
Date: 2018-09-25 11:05
Lang: es
Slug: citizenfour
Image: 2018/09/citizenfour.png
Tags: snowden, citizenfour, libertad
Title: Citizenfour

En enero de 2013, **Laura Poitras** comenzó a recibir correos electrónicos
cifrados firmados por un tal  **"Citizenfour"**, que le aseguraba tener
pruebas de los programas de vigilancia ilegales dirigidos por la NSA
en colaboración con otras agencias de inteligencia en todo el mundo.
Cinco meses más tarde, junto con los periodistas **Glenn Greenwald** y
**Ewen MacAskill** voló a **Hong Kong** para el primero de muchos encuentros
con un hombre anónimo que resultó ser **Edward Snowden**.
Para sus encuentros, viajó siempre con una cámara.
La película resultante es la historia que se desarrolla ante nuestros
ojos en este documental.

<video playsinline controls poster='{static}/wp-content/uploads/article/images/2018/09/preview-snowden.jpg'>
  <source src="https://archive.org/download/libreweb/citizenfour-spanish.webm" type="video/webm"/>
  <p>Lo siento, tu navegador no soporta vídeo en HTML5. Por favor, cambia o actualiza tu navegador web</p>
</video>

<p class="has-text-right">
  <small><strong>VideoTime: </strong>1h 48min 40sec</small>
</p>
