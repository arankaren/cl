Author: Jesús E.
Category: Tutorial
Date: 2018-08-22 10:52
Image: 2018/08/montar-android-en-hyperbola.png
Slug: montar-android-en-hyperbola
Tags: android, mtp, hyperbola, usb
Title: Montar Android en Hyperbola

En esta oportunidad se enseña cómo montar los dispositivos
Android en Hyperbola.

<video playsinline controls>
  <source src="https://archive.org/download/libreweb/android-uucp-480.webm" data-res="480" type="video/webm">
  <p>Lo siento, tu navegador no soporta vídeo en HTML5. Por favor, cambia o actualiza tu navegador web</p>
</video>

<p class="has-text-right">
  <small><strong>VideoTime: </strong>4min 54sec</small>
  <a href="https://archive.org/download/libreweb/android-uucp-480.webm">SD</a> - <a href="https://archive.org/download/libreweb/android-uucp.webm">HD</a>
</p>
