Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2019-03-17
Image: 2019/03/5g.png
Lang: es
Slug: contra-las-redes-5g
Tags: celular, datos, móvil, redes, salud, telefonía
Title: Contra las redes 5G

Las llamadas redes de quinta generación (abreviadas 5G) están siendo
impulsadas y propuestas por las grandes empresas de telecomunicaciones
con una gran agresividad. Con ellas se quiere reducir la latencia,
transmitir más datos por segundo y lograr una conectividad masiva de
dispositivos.

Sin embargo, poco hablan quienes publicitan estas tecnologías sobre los
grandes peligros que conllevan, entre otras cosas, para la salud, el
medio ambiente y la libertad individual. Para las empresas de
telecomunicaciones y gobiernos resulta muy rentable obtener tan grandes
cantidades de datos, porque sabrán constantemente lo que hace la gente.

La tecnología 5G supondrá una mucho mayor exposición a los campos
electromagnéticos de radiofrecuencia respecto a las ya existentes (2G,
3G, 4G, Wi-Fi...). Desplegar esta tecnología supone también grandes
costes económicos y ecológicos asociados a la construcción e instalación
de nuevas atenas, satélites y dispositivos.

Los [efectos de la radiación electromagnética para los seres
vivos](https://ehtrust.org/key-issues/cell-phoneswireless/5g-networks-iot-scientific-overview-human-health-risks/)
son desastrosos: daño oxidativo, roturas de
<abbr title="ácido desoxirribonucleico">ADN</abbr> de cadenas simple y
doble, reducción de metabolismo celular, aumento de la permeabilidad de
barrera hematoencefálica, reducción de melatonina, interrupción del
metabolismo de la glucosa del cerebro, generación de proteinas de
estrés... Esta nueva tecnología aumenta enormemente los niveles de dicha
radiación.

Las tecnologías móviles actuales **ya** suponen riesgos para la salud.
La [Agencia Internacional para la Investigación del Cáncer clasificó la
radiofrecuencia de los campos
electromagnéticos](https://www.iarc.fr/wp-content/uploads/2018/07/pr208_E.pdf),
asociados con el teléfono inalámbrico, como posible cancerígeno para
humanos.

[Cuatro mujeres jóvenes que llevaban el celular en el sujetador
desarrollaron cáncer de mama en el área donde llevaban el
celular](https://drmicozzi.com/breast-cancer-from-contact-with-cell-phones),
a falta de factores de riesgo como factores hereditarios o edad superior
a los cuarenta años concluyeron que la causa del cáncer era la radiación
de los celulares.

Se ha demostrado también que el [uso del celular afecta a la fertilidad
de los hombres](https://www.excelsior.com.mx/global/2016/02/04/1073076),
que [produce insomnio, dolores de cabeza y
confusión](http://www.curiosidadsq.com/2013/09/Radiacion-del-telefono-movil.html).
Existen muchos estudios que establecen una relación entre la exposición
prolongada a la radiación de los celulares; otros estudios de
mayor duración se están llevando a cabo en la actualidad.

Permitir el uso de una tecnología que aumenta los riesgos de salud es
extremadamente imprudente. Más aún teniendo en cuenta que sus
consecuencias afectarían a todos los seres vivos.

**Artículo Original:** [https://freakspot.net/contra-las-redes-5g/](https://freakspot.net/contra-las-redes-5g/)
