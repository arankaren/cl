Author: Jesús E.
Category: Opinión
Date: 2017-08-14 11:09
Image: 2017/08/openmailbox.png
Slug: sayonara-openmailbox
Tags: openmailbox, mail
Title: Sayonara OpenMailBox

OpenMailBox, llevaba operando desde 2012, pero a fecha de este artículo
año 2017, ha cambiado las políticas de sus servicios, lo que ha provocado
el descontento en la mayoría de sus usuarios, y la migración notoria
hacia otros servicios.

La comunicación con sus usuarias y usuarios no ha sido el punto fuerte
de este servicio.
Al principio sí que había un foro oficial que con el tiempo desapareció
sin más explicaciones.

La única vía de comunicación oficial de OpenMailBox era mediante su cuenta
de Twitter (una red social No libre y centralizada), y tampoco parecen
prodigarse demasiado en explicaciones.

Los cambios más notables consisten en, los que ya eran usuarios, tendrían
acceso sólo al correo mediante su web cliente. Por el contrario el
pagar o no por un servicio no es el problema, el asunto viene en las
siguientes líneas…

Su página de login desafortunadamente ejecuta **ECMAScript** (JavaScript)
No libre proveniente de Google (la API recaptcha, [freakspot][fsite]
cuenta a detalle sobre esta API), ese es el verdadero problema ya que
sin duda ahora los usuarios estarían a merced del posible ciber-espionaje
y va en contra de la [filosofía del Software Libre][gnu-philosophy].

## Reemplazos de servicios con Software Libre

+ [Posteo][posteo], de pago 1 EUR por mes,
+ [Riseup][riseup], requiere de una invitación, y de donaciones para su funcionamiento, y
+ [Otros servicios][other] que respetan la libertad.

Por el mal trato recibido y la poca profesionalidad de OpenMailBox solo puedo decir: ha sido un placer, pero adiós!

[fsite]: https://www.freakspot.net/como-explota-Google-con-CAPTCHAs
[gnu-philosophy]: https://www.gnu.org/philosophy/who-does-that-server-really-serve.es.html
[posteo]: https://posteo.de/
[riseup]: https://riseup.net/
[other]: https://www.fsf.org/resources/webmail-systems
