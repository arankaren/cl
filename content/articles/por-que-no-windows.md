Author: Jesús E.
Category: Opinión
Date: 2018-02-17 08:14
Image: 2018/02/question.png
Slug: por-que-no-windows
Tags: libre, libertad, gnu, linux-libre
Title: ¿Por qué no Windows?

Windows y Office funcionan bien, entonces: **¿cómo pueden ser tan malos?**

## Restricciones

Una copia legal de Windows es costosa, pero ¿qué obtienes realmente?
Ni Windows ni Office son verdaderamente vendidos, en realidad solo se
vende la licencia para usarlos.

Para usar esos productos, se debe aceptar **varias restricciones muy
fuertes**. En la mayoría de las licencias de Windows, no puedes conservar
el software cuando cambias de computador. A veces ni siquiera puedes
regalar el software. Microsoft te impone quién puede usar el software,
en qué computador y para qué propósitos; la lista de restricciones es
larga y en ocasiones aberrante.

## ¿Qué hay de la elección?

El software debería venir sin mecanismos para atar a los usuarios
a los productos de una compañía.

¿Por qué los documentos de Office son tan difíciles de exportar?
¿Por qué sus formatos están constantemente cambiando? ¿Por qué no
siquiera puedes desinstalar algunos programas? Si buscas el control,
los productos de Microsoft no son para usted.

## Sin código fuente

El código fuente (los detalles del funcionamiento de un programa) de
Windows y Office están ocultos, y además **nadie tiene permiso legal
para averiguar cómo funcionan**.

Si no tienes derecho a inspeccionar el código fuente (la descripción
del funcionamiento de un programa), no puedes pedirle a un informático
que modifique el software ni que evalúe cómo el software protege tu privacidad.

Y adivina qué: En el software que cuyo código fuente está disponible,
los programas maliciosos (como los malware o virus) no logran su objetivo,
es decir, aquí la seguridad no se compra por separado. La industria de
los antivirus en la cual Microsoft juega un papel crucial, prefiere que
tú uses Windows.

## Por una sociedad Libre

Un sociedad libre requiere Software Libre. Piensa en "Libre" como en
libertad, no de precio: la libertad para inspeccionar, aprender y
modificar el software que utilice.

Las computadoras se usan para compartir ideas, cultura e información.
Sin estas libertades sobre el software, estamos en riesgo de perder
el control sobre lo que compartimos.

Esto ya está sucediendo hoy, desde tecnologías evidentemente fastidiosas
como la [Gestión Digital de Restricciones][drm] (DRM) hasta las
completamente espantosas como la [Computación Confiable][tc] (TC).
El derecho que tiene cualquiera de participar en la cultura se está
viendo amenazado.

Si tienes que renunciar a tu libertad para usar un software, quizá
entonces no estés contento con el software privativo.

Fuente: [https://www.getgnulinux.org/es/windows/](https://www.getgnulinux.org/es/windows/)

**Noticias relacionadas:**

+ [Piden prisión para la dueña de un locutorio que tenía copias ilícitas de Windows instaladas][locutorio]

[drm]: https://www.gnu.org/proprietary/proprietary-drm.html
[tc]: https://www.gnu.org/philosophy/can-you-trust.es.html
[locutorio]: https://www.20minutos.es/noticia/3332612/0/prision-duena-locutorio-tenia-copias-ilicitas-windows-instaladas/
