Author: Jesús E.
Category: tutorials
Date: 2019-08-24 12:47
Image: 2019/08/clonar-disco.jpg
Lang: es
Slug: clonar-disco-duro-con-dd-en-gnu
Tags: dd, grabar disco duro,
Title: Clonar disco duro con dd en GNU

El programa `dd` nos permite convertir y copiar archivos,
pero no un simple copiado de archivos sino que `dd` también
sirve para clonar discos duros enteros.

> Este comando se ejecuta como superusuario.

#### Parámetros
Los parámetros más importantes son 3:

+ if: archivo de origen
+ of: archivo de destino
+ bs: límite de bytes que se leen y escriben cada momento, es decir velocidad de grabado.

Por supuesto, podéis ver más opciones con el comando `dd --help`.

#### Ejemplo

Antes de comenzar, hay que saber qué discos duros o memorias
USB hay conectadas al equipo.
Para ello, ejecutamos el comando `lsblk`.
Suponiendo que el disco duro que queremos clonar está en `/dev/sda`
y el disco duro de destino está en `/dev/sdb`, para realizar el
clonado debemos ejecutar el siguiente comando:

```bash
dd bs=1M if=/dev/sda of=/dev/sdb
```

Con la opción `bs=1M` estamos diciendo que la velocidad de
lectura y escritura se realicen en bloques de 1 Megabyte.
Cuanto más bajo, más lento y más seguro. Cuanto más alto más rápido,
pero nos arriesgamos a que no se copie bien.

Para crear una imagen ISO a partir de un CD se ejecutaría este comando:

```bash
dd if=/dev/cdrom of=/home/usuario/imagendeldisco.iso
```

Para guardar una ISO en un DVD se intercambiarían origen y destino:

```bash
dd if=/home/usuario/imagendeldisco.iso of=/dev/cdrom
```

El programa dd ofrece muchas posibilidades a la hora de trabajar con
discos duros, memorias externas y CDs o DVDs. Yo lo utilizo mucho y
la verdad es que es más cómodo que cualquier otro programa para grabar discos.
