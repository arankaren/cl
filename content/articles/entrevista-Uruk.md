Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2018-06-20 12:50
Image: 2018/06/uruk.png
Lang: es
Slug: entrevista-uruk
Tags: entrevista, GNU/Linux, proyecto, software libre, Trisquel, Uruk
Title: Entrevista sobre el proyecto Uruk

El Proyecto Uruk incluye muchas aplicaciones útiles y una distribución
de GNU/Linux basada en Trisquel. He entrevistado a Hayder Majid, una de
las iniciadoras, para conocer más.

**¿Puedes presentar brevemente el Proyecto Uruk y la distribución
Uruk GNU/Linux?**

El proyecto nació en 2016, pero la historia empezó días atrás de ese
año. Mi amigo alimiracle y yo estábamos pensando en crear una
distribución completamente libre para satisfacer nuestras necesidades y
compartirla con otras, así que nos reunimos con un pequeño equipo con la
misma idea y rápidamente nos unimos a él. El equipo dividió el trabajo
de forma incorrecta, y el proyecto falló. Después de eso, hasta 2016,
hicimos algunas aplicaciones como Uruk cleaner, el
[tema de iconos Masalla](https://www.gnome-look.org/content/show.php/Masalla+Icon+Theme?content=170321),
[UPMS](https://notabug.org/alimiracle/UPMS) y otras cosas,
así que decidimos hacer estos programas bajo un techo común. Lo llamamos
Proyecto Uruk e hicimos una distribución completamente libre con el
mismo nombre que respeta la libertad y la privacidad de las usuarias.

**¿Cuáles son los antecedentes de las contribuidoras al proyecto y sus
motivaciones?**

Tenemos muchas personas que nos ayudan en el Proyecto Uruk, y que
comparten nuestras metas y motivaciones sobre el software libre, pero
principalmente tenemos dos tipos de cotribuidoras: primero, las
integrantes del equipo del proyecto; tienen proyectos bajo el Proyecto
Uruk o desarrollados en uno o más de los subproyectos de Uruk, que
hicieron un cambio sustancial en el Proyecto Uruk, las integrantes
activas del equipo son las siguientes:

- *Ali Abdul Ghani (Ali Miracle)*: programador y fundador del Proyecto
  Uruk y Desarrollador de Uruk GNU/Linux y muchos otros subproyectos.
- *Hayder Majid (Hayder Ctee)*: ingeniero informático, programador y
  diseñador, fundador del Proyecto Uruk y Uruk GNU/Linux, y muchos
  otros.
- *Rosa*: Programadora, empaquetadora, administradora del servidor y
  desarrolladora del Proyecto Uruk y Uruk GNU/Linux.
- *Ahmed Nourllah*: Programador y desarrollador del Proyecto Uruk y
  desarrollador principal del wiki de Uruk.

El segundo es otro tipo de contribuidoras, que pueden apoyar el proyecto
traduciendo o escribiendo código, empaquetando, etc.

**Como usuaria de GNU/Linux, lo ejecuto en un viejo portátil Thinkpad
X60 y tengo aún así una experiencia de uso fluida. Gracias por hacer
el tema de iconos Masalla que mencionaste antes. Siendo Uruk GNU/Linux
una distribución basada en Trisquel, me pregunto por qué no elegisteis
Debian para basaros en esta o contribuisteis directamente a Trisquel en
vez de crear una nueva distribución.**

Como dijo Rosa, «no puedes hacer que todo el mundo coma un tipo de
tarta».

Elegimos Trisquel porque creemos en la filosofía del software libre.
Trisquel es una distribución completamente libre que cumple con nuestros
objetivos, pero no con nuestras necesidades, así que hicimos nuestra
distribución. Si probaste Uruk y Trisquel antes, encontrarás algunas
diferencias entre ellas porque Uruk es más personalizable y tiene muchas
aplicaciones creadas por el equipo de Uruk (como upms, ucc y otros
programas). También tratamos la opinión de la comunidad como la base de
nuestras publicaciones y creo que influimos a Trisquel, pero
indirectamente: después de todo, puedes verlo en Trisquel 8, que usa el
[escritorio MATE](https://es.wikipedia.org/wiki/MATE) como el entorno de escritorio predeterminado y VLC como
un reproductor multimedia [Uruk hizo eso antes].

**Estáis tomando medidas para ganar el reconocimiento de la _Free
Software Foundation_ [Fundación del Software Libre] y que sea añadida a
la lista de distribuciones que son completamente libres**

Sí, realizamos un paso serio para añadir Uruk GNU/Linux a la lista de
distribuciones libres de GNU, pero tarda mucho tiempo, o como dijo mi
amigo Ali «mil años [sonrisa]».

<figure>
    <a href="{static}/wp-content/uploads/article/images/2018/06/Uruk-2.0.captura.png">
    <img src="{static}/wp-content/uploads/article/images/2018/06/Uruk-2.0.captura.png" alt="Uruk" width="1025" height="768">
    </a>
    <figcaption class="wp-caption-text">Captura de pantalla de Uruk
    GNU/Linux 2.0</figcaption>
</figure>


**La última publicación fue GNU/Linux Uruk 2.0, basada en Trisquel 8.
¿Cuáles son vuestros planes para el futuro?**

Tenemos muchos planes para el futuro, como añadir más sabores a nuestras
publicaciones de Uruk GNU/Linux, mejorar la infraestructura del
proyecto, añadir nuevas aplicaciones creadas por el equipo del proyecto
y algunas otras sorpresas [guiño].
