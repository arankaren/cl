Author: Jesús E.
Category: tutorials
Date: 2019-11-11 08:41
Image: 2019/11/hyperbola-base.jpg
Lang: es
Slug: guia-de-instalacion-de-hyperbola
Tags: hyperbola, guia
Title: Guia de instalación de Hyperbola [Modo Legacy/MBR-DOS]

[TOC]

Primero debemos construir una unidad de instalación de Hyperbola,
puede ser en una memoria [USB][usb-boot]{:target='_blank' rel='noopener noreferrer'}
o en [CD][cd-boot]{:target='_blank' rel='noopener noreferrer'}.

También existe un [vídeo de instalación][video-guide]{:target='_blank' rel='noopener noreferrer'}
que se irá actualizando.

### Primeros pasos

Configuración teclado temporal

    # loadkeys es

Comprobamos si hay conexión a Internet

    # ping -c 3 gnu.org

Particionar disco

    # cfdisk

- En el caso de la partición elegida como **swap** ir a la opción "Type" y seleccionar **82 (Linux swap)** de la lista.
- En el caso de la partición elegida como **/boot**, se selecciona **"bootable"**

ejemplo:

    :::bash
    sda1=/boot
    sda2=/
    sda3=/home
    sda4=swap

    Recomendaciones:
    /boot   = 300MB
    /       = 40GB
    /home   = personalizado
    swap    = igual a la RAM

#### Conexión por Wi-fi

Detectar `wifi`

    # iw dev

Activar dispositivo de Red

    # ip link set <nombre-del-dispositivo> up

Activar Internet con `wpa_supplicant`

    # wpa_supplicant -B -i <nombre-de-dispositivo> -c <(wpa_passphrase "ssid" "psk")

Renovar IPv4 con `dhcpcd`

    # dhcpcd <nombre-de-dispositivo>

### Formateo de Particiones

- En `/boot` se utilizará **ext4**

        # mkfs -t ext4 /dev/sda1

- En `/` se utilizará **ext4**

        # mkfs -t ext4 /dev/sda2

- En `/home`, se utilizará **ext4**

        # mkfs -t ext4 /dev/sda3

- En `swap`, se utilizará **mkswap**

        # mkswap /dev/sda4

- Activamos la partición swap

        # swapon /dev/sda4

### Organización de Particiones

- Montamos root en /mnt

        # mount /dev/sda2 /mnt

- Creamos los directorios de las otras particiones

    - boot

            # mkdir /mnt/boot

    - home

            # mkdir /mnt/home

- Montamos las particiones correspondientes

    - Montar boot

            # mount /dev/sda1 /mnt/boot

    - Montar home

            # mount /dev/sda3 /mnt/home

### Instalación del Sistema Base

Actualizamos las llaves de la iso:

    # pacman -Sy hyperbola-keyring

Instalamos los paquetes base:

    # pacstrap /mnt base base-devel grub-bios wpa_supplicant iw kernel-firmware ldns xenocara-input-synaptics

> Instalar `xenocara-input-synaptics` Solo en laptops (ordenadores portátiles)

### Configuración Principal

- Generar el archivo fstab

        # genfstab -U -p /mnt >> /mnt/etc/fstab

- Efectuar chroot y configurar el sistema base

        # arch-chroot /mnt

- Establecer nombre del equipo para esto tenemos que editar el archivo `/etc/hostname`:

    Ejemplo:

        # echo hyperpc > /etc/hostname

- Configurar idioma

        # ln -s /usr/share/zoneinfo/America/Lima /etc/localtime

- Actualizar hora del sistema (Opcional)

        # hwclock --systohc --utc

- Activar nuestra localización, descomentar

        # sed -e 's/^#es_ES.UTF-8 UTF-8/es_ES.UTF-8 UTF-8/g' -i /etc/locale.gen

- Establecer las preferencias de localización

        # echo LANG=es_ES.UTF-8 > /etc/locale.conf

- Generamos la localización

        # locale-gen

- Como la configuración regional, debe configurar el mapa de teclas en el archivo `/etc/conf.d/keymaps`

        # sed -e 's/^keymap="us"/keymap="es"/g' -i /etc/conf.d/keymaps

- Instalación del Grub

        # grub-install --target=i386-pc --recheck /dev/sda

- Creamos el archivo grub.cfg

        # grub-mkconfig -o /boot/grub/grub.cfg

- Editar ramdisk

        # nano -w /etc/mkinitcpio.conf
        -----------------------------
        HOOKS="base udev autodetect modconf block filesystems keyboard fsck"

- Generar ramdisk

        # mkinitcpio -p linux-libre-lts

- Establecer contraseña del usuario root

        # passwd

- Configurar [wpa_supplicant][wpa_link]{:target='_blank' rel='noopener noreferrer'}

        # nano -w /etc/wpa_supplicant/wpa_supplicant.conf

    y dentro:

        :::bash
        network = {
            ssid="lynx"
	        psk="your-pass"
        }

    Agregar a servicio por defecto

        # rc-update add wpa_supplicant default

- [DHCPCD][dhcpcd_link]{:target='_blank' rel='noopener noreferrer'}

    Agregar a servicio por defecto

        # rc-update add dhcpcd default

- Cerrar chroot

        # exit

- Desmontar particiones

        # umount '/mnt/{boot,home,}'

- Reiniciar

        # reboot

### Creación de usuario

Crear el grupo de usuario, por ejemplo: `libre`

    # groupadd libre

Creamos el usuario `freedom` y lo agregamos a los grupos básicos

    # useradd -m -G audio,disk,games,http,input,lp,network,optical,power,scanner,storage,sys,video,wheel -g libre -s /bin/bash freedom

- Asignamos contraseña

        # passwd freedom

- Editamos el archivo `/etc/sudoers`

        # sed -i /etc/sudoers -e 's/^# %whell ALL=(ALL) ALL/%whell ALL=(ALL) ALL/g'

- Reiniciamos el equipo

        # reboot

- Actualización del sistema

        $ sudo pacman -Syu

### Interfaz gráfica BASE

#### Instalar paquete de vídeo (según marca de su tarjeta de vídeo)

Comprobar marca:

    # lspci | grep -e VGA

Instalar una de ellas dependiendo de la marca:

AMD:

    # pacman -S xorg-video-amdgpu

Ati:

    # pacman -S xenocara-video-ati

Intel:

    # pacman -S xorg-video-intel

Nvidia:

    # pacman -S xorg-video-nouveau

Vesa (genérico):

    # pacman -S xenocara-video-vesa

#### Componentes Xenocara

    # pacman -S xenocara-server xenocara-xinit xenocara

#### Mesa demos

    # pacman -S mesa mesa-demos

#### Ajuste en el idioma de teclado para Xenocara

La sintaxis de los archivos de configuración de X se explica
en [Xenocara#Configuration][xe-conf]{:target='_blank' rel='noopener noreferrer'}.
Este método crea la configuración para todo el sistema, que se mantiene después de los reinicios.

He aquí un ejemplo:

    # nano -w /etc/X11/xorg.conf.d/00-keyboard.conf

y dentro colocar:

    :::bash
    Section "InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout"  "es,us"
        Option "XkbModel"   "pc105"
        Option "XkbVariant" "deadtilde,dvorak"
        Option "XkbOptions" "grp:alt_shift_toggle"
    EndSection

XkbOptions puede recibir varios parámetros por ejemplo:

`"grp:alt_shift_toggle,compose:rwin,lv3:ralt_switch,numpad:pc"`

Tenemos 2 diseños de teclado: español (es) e inglés (us), y que para pasar de uno
al otro solo basta usar la combinación de teclas `ALT+SHIFT`

- Comprobar la configuración del teclado:

        # setxkbmap -print -verbose 10

- Tipografías

        # pacman -S ttf-liberation ttf-bitstream-vera ttf-dejavu ttf-droid

A continuación, procederemos a crear las carpetas personales.
En caso de tener planeado instalar **GNOME** o **PLASMA KDE** como entorno de escritorio,
obvie este paso, puesto que la instalación de **GNOME** o **PLASMA KDE** generan de forma
automática los directorios.

- Instalación del generador de directorios:

        # pacman -S xdg-user-dirs

- Creación automática de directorios:

        # xdg-user-dirs-update

#### Soporte de audio

Instalar `pulseadio`

    # pacman -S pulseaudio pulseaudio-alsa alsa-utils pavucontrol

Agregar el servicio de audio a por defecto

    # rc-update add alsasound default

#### Configurar pulseaudio

    # sed -e 's/^; autospawn = yes/autospawn = yes/g' -i /etc/pulse/client.conf

### Entornos de escritorio soportados oficialmente

#### Instalar MATE

El entorno de escritorio MATE es la continuación de GNOME 2 (Basado en Gnome 2).
Provee un entorno intuitivo y atractivo. MATE está siendo desarrollado activamente
para añadir apoyo para tecnologías nuevas, y a la misma vez preservar la experiencia
tradicional de un escritorio.

- Instalación:

        # pacman -S mate mate-extra

Donde:

+ mate: Contiene el entorno de escritorio básico y aplicaciones necesarias para la experiencia estándar de MATE.
+ mate-extra: Contiene un conjunto de paquetes y herramientas opcionales, como un salvapantallas, una calculadora,
un editores y otras aplicaciones no problemáticas que van bien con el escritorio MATE.

#### Instalar XFCE

Xfce es un entorno de escritorio ligero para sistemas tipo UNIX.
Su objetivo es ser rápido y usar pocos recursos del sistema,
sin dejar de ser visualmente atractivo y fácil de usar.

- Instalación

        # pacman -S xfce4 xfce4-goodies

Donde:

+ xfce4: es el entorno de escritorio basico Grupo de paquetes que contiene.
+ xfce4-goodies: es un grupo de paquetes adicionales, como plugins para el panel,
notificaciones y otras herramientas del sistema.

#### Instalar LXDE

LXDE es un entorno de escritorio libre. Su nombre proviene de «Lightweight X11 Desktop Environment»,
que en español significa: Entorno de escritorio X11 ligero.

- Instalación

        # pacman -S lxde

#### Instalar KDE Plasma

KDE es un proyecto de software que actualmente comprende un
entorno de escritorio conocido como Plasma, una colección de librerías
y frameworks (KDE Frameworks) y también una gran cantidad de
aplicaciones (KDE Applications).

El entorno de escritorio creado por KDE principalmente para sistemas GNU/Linux,
KDE Plasma 5, es el sucesor de KDE Plasma Workspaces y se lanzó
inicialmente el 15 de julio de 2014.

- Instalación

        # pacman -S plasma kde-applications plasma-wayland-session

Donde:

+ plasma: contiene el grupo de paquetes que instalaran el D.E. con algunas aplicaciones y herramientas basicas.
+ kde-applications: instala todas las aplicaciones de KDE contenidas en el grupo.
+ plasma-wayland-session: Para habilitar el soporte para Wayland en Plasma.

#### ¿Cómo iniciar Xenocara?

- Escribir un archivo `~/.xinitrc` (opción 1)

    Descomentar el escritorio que usted instaló, ejemplo de archivo `~/.xinitrc`:

        :::bash
        #!/bin/sh
        #
        # ~/.xinitrc
        #
        # Executed by startx (run your window manager from here)
        #
        # exec enlightenment_start
        # exec i3
        # exec mate-session
        # exec xmonad
        # exec startlxqt
        # exec startlxde
        # exec awesome
        # exec bspwm
        # exec gnome-session
        # exec gnome-session --session=gnome-classic
        # exec startkde
        # exec startxfce4
        # exec startfluxbox
        # exec openbox-session
        # exec cinnamon-session
        # exec pekwm
        # exec catwm
        # exec dwm
        # exec startede
        # exec icewm-session
        # exec jwm
        # exec monsterwm
        # exec notion
        # exec startdde #deepin-session

    Luego desde una tty, usted puede ejecutar `startx` y se iniciará su escritorio.

- Instalar gestor de inicio de sesión (opción 2)

    Ejemplo: `lightdm`

        # pacman -S lightdm lightdm-gtk-greeter

    Agregar servicio lightdm

        # rc-update add lightdm default

- Reiniciar

        # reboot

### UTILIDADES

#### Discos

- gvfs para montar discos

        # pacman -S gamin gvfs

#### Red

- dhcpcd-ui para gestión de IP's

        # pacman -S dhcpcd-ui

#### Gestor de claves

- gnome-kering

        # pacman -S gnome-keyring

#### Applet de volumen

- Ícono de volumen

        # pacman -S volumeicon

#### Sincronizar el Horario Local

Instalar NTP

    # pacman -S ntp

Sincronizar Hora

    # ntpdate -u hora.roa.es

#### Compresores de archivos

Normalmente nos topamos con carpetas comprimidas en ZIP, RAR y/u otro formato que
se suelen intercambiar en Internet. En muchos entornos de escritorio, suelen
incluir el suyo (File Roller en GNOME, Engrampa en MATE, Ark en KDE y XArchiver
en XFCE/LXDE). Para mejorar la funcionalidad de estos compresores de archivos,
le añadiremos el soporte para 7Z, RAR, ZIP y otros.

- GZip (conocidos por la extensión “.tar.gz”):

        # pacman -S zlib haskell-zlib

- BZip2:

        # pacman -S bzip2

- RAR:

        # pacman -S unar

- 7Zip:

        # pacman -S p7zip lrzip

- ZIP:

        # pacman -S zip libzip unzip

#### Detector de particiones

- Utilitario Udisk:

        # pacman -S udevil autofs

- Lectura y escritura de sistemas de archivos NTFS:

        # pacman -S ntfs-3g

- Lectura y escritura de sistema de archivos FAT32:

        # pacman -S fatsort exfat-utils dosfstools

- Lectura y escritura de sistemas de archivos XFS:

        # pacman -S xfsprogs

#### Soporte multimedia
Para poder reproducir archivos multimedia, es necesario poder tener los códecs y el
reproductor. Para ello, procederemos a instalar los códecs ffmpeg y gstreamer,
además de los reproductores.
Aquí les sugiero algunos reproductores que les puede resultar útiles.

- Códecs:

        # pacman -S ffmpeg gstreamer gst-libav gst-plugins-bad gst-plugins-good gst-plugins-ugly gst-plugins-base gstreamer-vaapi gst-transcoder ffms2 x264 libvorbis libvpx libtheora opus vorbis-tools

- Reproductor Audacious:

        # pacman -S audacious

- Reproductor SMPlayer:

        # pacman -S smplayer smplayer-themes smplayer-skins

- Reproductor VLC:

        # pacman -S vlc

- Reproductor MPV:

        # pacman -S mpv

- Visor de imagenes ligero

        # pacman -S viewnior

- Visor PDF

        # pacman -S epdfview

#### Aplicaciones UXP
Existen navegadores incluidos por los entornos de escritorio como GNOME con Epiphany
o KDE con Konqueror. No obstante, gracias a los desarrolladores Hyperbola tenemos
**[Iceweasel-UXP][iceweasel-uxp]{:target='_blank' rel='noopener noreferrer'}** un
navegador web,
**[Iceape-UXP][iceape-uxp]{:target='_blank' rel='noopener noreferrer'}** suite de internet
y  un gestor de correos llamado **[Icedove-UXP][icedove-uxp]{:target='_blank' rel='noopener noreferrer'}**.

- Iceweasel-UXP:

        # pacman -S iceweasel-uxp iceweasel-uxp-l10n-es-es

- Iceape-UXP:

        # pacman -S iceape-uxp iceape-uxp-l10n-es-es

- Icedove-UXP:

        # pacman -S icedove-uxp icedove-uxp-l10n-es-es

#### LibreOffice
Por lo general, a la hora de usar un Sistema Operativo, por lo menos se tiene una suite de oficina.
En GNU/Linux, se acostumbra a tener una. Felizmente en Hyperbola, Libreoffice se presenta es su versión estable.
Lo único que necesitaríamos es efectuar el siguiente comando:

    # pacman -S libreoffice-still libreoffice-still-l10n-es

##### Corrección de ortografía
Para revisar la ortografía necesitará hunspell y un diccionario de hunspell (como hunspell-es, hunspell-en, etc.)

    # pacman -S hunspell hunspell-es

##### Reglas de división de palabras
Para disponer de las reglas de división también necesitará hyphen + un conjunto de reglas (hyphen-en, hyphen-de)

    # pacman -S hyphen hyphen-es

##### Sinónimos
Para la opción Sinónimos necesitará `mythes` + un libro de sinónimos de mythes (`mythes-en mythes-es`)

    # pacman -S mythes mythes-es

#### Seguridad
La seguridad es importante al navegar por Internet es por ello que Hyperbola provee de una herramienta
llamada **[firejail][firejail]{:target='_blank' rel='noopener noreferrer'}**
en combinación con una interfaz gráfica
**[firetools][firetools]{:target='_blank' rel='noopener noreferrer'}**.

    # pacman -S firejail firetools

#### Comunicación
La comunicación a través de Internet es requerida a día de hoy. En Hyperbola tenemos programas para la comunicación a través de Internet:

- Gajim

        # pacman -S gajim python2-axolotl

- Tox
    - qtox:

            # pacman -S qtox

    - toxic:

            # pacman -S toxic

#### Identificando keycodes

La utilidad `showkey` informa los códigos de teclas para la consola virtual.
showkey espera a que se presione una tecla y, si no hay ninguna durante 10 segundos,
se cierra. Para ejecutar showkey necesita estar en una consola virtual,
no en un entorno gráfico. Ejecute el siguiente comando:

    # showkey --keycodes

[video-guide]: https://lablibre.tuxfamily.org/hyperbola-gnu-linux-libre-base/
[firejail]: https://github.com/netblue30/firejail
[firetools]: https://l3net.wordpress.com/projects/firejail/#firetools
[usb-boot]: https://wiki.hyperbola.info/doku.php?id=en:guide:beginners#write_the_image_to_your_usb
[cd-boot]: https://wiki.hyperbola.info/doku.php?id=en:guide:beginners#burn_the_image_to_your_optical_disk
[wpa_link]: https://wiki.archlinux.org/index.php/WPA_supplicant
[dhcpcd_link]: https://wiki.archlinux.org/index.php/Dhcpcd
[iceweasel-uxp]: https://wiki.hyperbola.info/iceweasel-uxp
[iceape-uxp]: https://wiki.hyperbola.info/iceape-uxp
[icedove-uxp]: https://wiki.hyperbola.info/icedove-uxp
[xe-conf]: https://fedoraproject.org/wiki/Input_device_configuration#xorg.conf.d
