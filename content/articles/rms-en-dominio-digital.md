Author: Jesús E.
Category: Opinión
Date: 2018-06-09 10:07
Image: 2018/06/RMS-Dominio-Digital.png
Slug: rms-en-dominio-digital
Tags: GNU/Linux
Title: RMS en Dominio digital

El pasado 30 de mayo, [Richard M. Stallman][rms] se presentó en Argentina
en el programa radial Dominio Digital. Dado que hay bastantes
puntos importantes que se tomaron en cuenta durante el programa
aquí os dejamos una copia de la Entrevista en vídeo.

**Dominio Digital** es un programa sobre informática, emitido en Televisión
Argentina entre 1996 y 2011. En el año 2018 volvió en formato de radio.

Sus integrantes son **Claudio Regis** (Conductor) y **Daniela Gastaminza**,
**Alejandro Ponike** y **Daniel "Chacal"** Sentinelli (columnistas).

<video playsinline controls poster='{static}/wp-content/uploads/article/images/2018/06/rms-screen-1.jpg'>
  <source src="https://archive.org/download/libreweb/StallmanenDominioDigital.webm" type="video/webm"/>
  <p>Lo siento, tu navegador no soporta vídeo en HTML5. Por favor, cambia o actualiza tu navegador web</p>
</video>

<p class="has-text-right">
  <small><strong>VideoTime: </strong>56min 07sec</small>
</p>

[rms]: https://es.wikipedia.org/wiki/Richard_Stallman
