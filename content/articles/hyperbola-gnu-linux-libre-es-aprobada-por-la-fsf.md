Author: Jesús E.
Category: Noticias
Date: 2018-12-08 11:25
Image: 2018/12/hyperbola-gnu-freedom.png
Slug: hyperbola-gnu-linux-libre-es-aprobada-por-la-fsf
Tags: fsf, hyperbola, gnu
Title: Hyperbola GNU/Linux-libre es aprobada por la FSF

La distribución **Hyperbola GNU/Linux-libre** ha sido aceptada
por la **Free Software Foundation** (FSF) en su lista de
distribuciones 100% software libre.

<figure class="has-text-center is-table">
    <img src="https://static.fsf.org/nosvn/images/hyperbola_logo.png" alt="Logo de Hyperbola" height="50" width="276">
    <figcaption>Hyperbola GNU con Linux libre</figcaption>
</figure>

Después de varios meses, **Hyperbola GNU/Linux-libre** una
distribución basada en **Arch GNU/Linux** y con la estabilidad
de **Debian** ahora es parte de [lista de distribuciones libres][distros-libres]
siendo avalada por la FSF.

<figure class="has-text-center is-table">
    <img src="https://static.fsf.org/common/img/logo-new.png" alt="Logo de FSF">
    <figcaption>Fundación de Software Libre (FSF por sus siglas en inglés)</figcaption>
</figure>

**John Sullivan** director ejecutivo de la FSF dijo al respecto:

>En un mundo donde los sistemas operativos privativos
>están en constante expansión en términos de abuso de sus
>propios usuarios, el añadir otra distribución a la lista
>de sistemas completamente libres es bienvenido. Hyperbola
>representa otro hogar seguro para las personas que busquen
>un control completo de su computación.

**André Silva**, co-fundador y desarrollador del proyecto Hyperbola menciona al respecto:

>Hyperbola es una distribución totalmente libre basada en
>snapshots de Arch y desarrollo de Debian sin software no libre,
>documentación o cualquier tipo de soporte para la instalación
>o ejecución de software no libre. A diferencia de Arch, que es
>una distribución de lanzamiento continuo, Hyperbola es una
>solución a largo plazo centrada en la estabilidad y la seguridad
>inspirado en Debian y Devuan.

**Donald Robertson**, gerente de licencias y cumplimiento de la FSF agregó:

>Fue un placer trabajar con el equipo detrás de Hyperbola
>durante todo este proceso. Realmente van más allá de lo
>que respecta a los derechos de sus usuarios.

**Hyperbola** es la primera distribución completamente libre nacida en **FISL17**
(Porto Alegre, Brasil). Tienes más información en la propia web de [Hyperbola][hypersite],
desde donde podrás [descargar][download] las imágenes para
[instalar]({filename}/articles/curso-de-instalacion-de-hyperbola.md)
o probar la distro en modo "live".

[distros-libres]: https://www.gnu.org/distros/free-distros.es.html
[download]: https://www.hyperbola.info/download/
[hypersite]: https://www.hyperbola.info
