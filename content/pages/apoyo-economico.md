Author: Jesús E.
Date: 2017-01-22 18:33
Lang: es
Modified: 2018-04-29 22:48
Slug: apoyo-economico
Status: published
Title: Apoyo económico

Desde su nacimiento, esta página ha usado solo software libre y ha
estado libre de publicidad. Sin embargo, para mantener un sitio web como
este se requiere tiempo y dinero. Las
[donaciones]({filename}/pages/donaciones.md) son para
fomentar su desarrollo y mejora.

Puedes eligir entre diferentes formas de realizar donaciones.

## Liberapay

<a href="https://liberapay.com/ConocimientosLibres/donate" role="button"><img alt="Dona" src="{static}/wp-content/uploads/pages/images/donar.svg"></a>
