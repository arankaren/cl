Author: Jorge Maldonado Ventura
Date: 2018-03-29 00:29
Lang: es
Slug: donaciones
Status: hidden
Title: Donaciones

<table>
    <tr>
        <th>Fecha</th>
        <th>Donante</th>
        <th>Moneda</th>
        <th>Cantidad</th>
    </tr>
    <tr>
        <td>2018-02-12</td>
        <td>Anónima</td>
        <td>Faircoin</td>
        <td>0,008</td>
    </tr>
</table>
