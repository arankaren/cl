Author: Jesús E.
Date: 2016-07-28 17:43
Lang: eo
Modified: 2017-04-27 16:15
Slug: acerca-de
Status: published
Title: Pri

## Kio estas Libera Kono?

Conocimientos Libres estas TTT-platformo dediĉita al la dissendo
de Scio en la Komputila Mondo, kiu implicas la tutmondan problemon
pri la programaro kaj la maniero en kiu ĝi estas distribuata;
Nia ĉefa objektivo estas la Liberigo de homoj de la uzo de
PRIVATIVAJ iloj kaj anstataŭigi ilin per liberaj iloj, ĉar nur
tiel ni garantias ke scio daŭrigos tutmonde kaj neniam
estos malkonfirmita.

## Kunlaborado

Vi povas skribi artikulojn por ĉi tia retpaĝaro, traduki aŭ kunlabori
kun la programado. En la
[legumino da fonta kodo da retpaĝaro](https://notabug.org/heckyel/cl/src/master/README.markdown#colaboraci%C3%B3n)
estas la informo pri kiel fari ĝin.
