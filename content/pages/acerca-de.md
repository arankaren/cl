Author: Jesús E.
Date: 2019-02-04 08:03
Lang: es
Modified: 2019-02-04 09:03
Slug: acerca-de
Status: published
Title: Acerca

## ¿Qué es Conocimientos Libres?

Conocimientos Libres es una plataforma Web dedicada a la distribución de
Conocimientos en el Mundo Informático, que involucra la problemática
mundial sobre el software y la manera en que se distribuye; tenemos como
objetivo principal la Liberación de las personas del uso de herramientas
PRIVATIVAS y reemplazarlos por herramientas LIBRES, pues solo de esta
manera garantizaremos que el conocimiento siga difundiéndose mundialmente
y jamás sea negado.

## ¿Qué es Software Libre?

Software libre significa que los usuarios tienen la libertad de ejecutar,
copiar, distribuir, estudiar, modificar y mejorar el software.

El software libre es una cuestión de libertad, no de precio. Para
entender el concepto, debe pensarse en «libre» como en «libertad de
expresión», no como en «cerveza gratis».

Más precisamente, software libre significa que los usuarios de un
programa tienen las [cuatro libertades esenciales][libertades]:

+ La libertad de ejecutar el programa como lo desee, con cualquier
  propósito (libertad 0).
+ La libertad de estudiar el funcionamiento del programa y adaptarlo
  a sus necesidades (libertad 1). El acceso al código fuente es un
  prerrequisito para esto.
+ La libertad de redistribuir copias para ayudar a los demás (libertad 2).
+ La libertad de mejorar el programa y de publicar las mejoras,
  de modo que toda la comunidad se beneficie (libertad 3).
  El acceso al código fuente es un prerrequisito para esto.

Debido a la evolución de la tecnología y del uso de la red, estas libertades
son ahora aún más importantes que en 1983.

<video playsinline controls="controls" poster="{static}/wp-content/uploads/pages/images/2017/03/poster.png">
  <source src="https://audio-video.gnu.org/video/TEDxGE2014_Stallman05_LQ.webm" type="video/webm">
  <track kind="captions" src="{static}/wp-content/uploads/pages/subtitles/2017/03/english.vtt" srclang="en" label="English">
  <track kind="captions" src="{static}/wp-content/uploads/pages/subtitles/2017/03/spanish.vtt" srclang="es" label="Español" default="">
  <track kind="captions" src="{static}/wp-content/uploads/pages/subtitles/2017/03/portuguese.vtt" srclang="pt" label="Portuguese">
  <p>Lo siento, tu navegador no soporta vídeo en HTML5. Por favor, cambia o actualiza tu navegador web</p>
</video>

## Colaboración

Puedes escribir un artículo para este sitio web, hacer una
traducción o colaborar en el desarrollo. La información sobre cómo
hacerlo, se encuentra en el
[README del código fuente de la web][colaboracion].

[colaboracion]: https://libregit.org/heckyel/cl/src/branch/master/README.md#colaboraci%C3%B3n
[libertades]: https://www.gnu.org/philosophy/free-sw.html
