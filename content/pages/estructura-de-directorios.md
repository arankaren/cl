Author: Jorge Maldonado Ventura
Date: 2018-01-28 17:35
Lang: es
Slug: estructura-de-directorios
Status: hidden
Title: Estructura de directorios

## Ficheros del directorio raíz

- `LICENSE`. La licencia de software libre del proyecto.
- `Makefile`. Archivo para la automatización de tareas.
- `pelicanconf.py`. Configuración local del proyecto.
- `publishconf.py`. Configuración en producción de la web.

Si hace falta cambiar algún parámetro de configuración genérico, se debe
editar el archivo `pelicanconf.py`. De tratarse de un ajuste que solo
necesita aplicarse durante la generación final, debe añadirse a
`publishconf.py`. Ejecuta `make` sin parámetros en este dictorio para
ver todas las opciones de `Makefile` disponibles.

## Directorios

- `content`. Contenido de la web: artículos, páginas, imágenes, etc.
- `cl-theme`. Diseño de la página web.
- `plugins`. Complementos para Pelican.
- `output`. El directorio donde se suele generar la página web.

Algunos de estos directorios se explican con más detalle a continuación.

### `content`

Aquí se guarda todo el contenido de la web. Todos los artículos se
encuentran en este directorio, incluidas las traducciones a artículos.
Los archivos de traducción indican el código del idioma antes de la
extensión `.md`. Si existe un artículo llamado `texto-sobre-algo.md`,
entonces la traducción al esperanto, por ejemplo, se debe llamar
`texto-sobre-algo.eo.md`. El idioma principal de la web es el
castellano.

Además de los artículos hay otro tipo de contenido que se organiza en
directorios:

- `asciicast`. Vídeos realizados con asciinema.
- `css`. El código <abbr title="Cascading Style Sheets">CSS</abbr>.
- `fonts`. Fuentes tipográficas.
- `js`. Código JavaScript.
- `pages`. Páginas que se encuentran en la barra lateral.
- `wp-content`. Contiene todo lo que no encaja en ningún otro sitio. En
  gran parte contiene imágenes. Los archivos se han de ubicar en la
  carpeta correspondiente al año y al mes en el que se añaden. Por
  ejemplo, el directorio `/wp-content/uploads/article/images/2018/01/`
  contiene los archivos de enero de 2018.
- `vendor`. Contiene el programa `aplaylist`, `play`, `plyr` y `form-comments`
  correspondiente a libravatar y un formulario en PHP que
  envía los comentarios por correo al dueño del sitio.

El código CSS, JS y las fuentes aquí encontrados no aparecen en todos
los artículos. Por esta razón, se ubican aquí (y no en `cl-theme`)y
son cargados cuando un artículo los necesita usando los complementos
pelican-css y pelican-js

### `cl-theme`

Contiene varias carpetas:

- `static`. Aquí se encuentran los archivos estáticos (como imágenes,
  fuentes tipográficas, JavaScript, CSS)
- `templates`. Aquí están las plantillas que usan el lenguaje Jinja2.
  Puedes aprender más sobre los archivos de plantillas de Pelican y cómo
  acceder a sus variables en [su
  documentación](http://docs.getpelican.com/en/stable/themes.html) (solo
  está disponible en inglés).
- `translations`. Aquí se ubican las traducciones del tema de la página.

Además están los archivos `Makefile`, que contiene las intrucciones más
comunes para trabajar con traducciones; `babel.cfg`, la configuración de
traducciones; y `message.po`, el catalogo de mensajes traducibles.

### `plugins`

Para aprender cómo funcionan los complementos en Pelican, consulta [su
documentación](http://docs.getpelican.com/en/stable/plugins.html)
(solo está disponible en inglés).
