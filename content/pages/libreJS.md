Author: Jorge Maldonado Ventura
Date: 2017-02-16 19:02
Modified: 2017-08-28 11:03
Slug: librejs
Status: hidden
Title: LibreJS

<table id="jslicense-labels1">
    <tr>
        <td><a href="../vendor/aplaylist/videoplaylist.js">videoplaylist.js</a></td>
        <td><a href="http://www.gnu.org/licenses/gpl-3.0.html">GPL-3.0</a></td>
        <td><a href="../vendor/aplaylist/videoplaylist.js">videoplaylist.js</a></td>
    </tr>
    <tr>
        <td><a href="../theme/js/navbar-burger.js">navbar-burger.js</a></td>
        <td><a href="http://www.gnu.org/licenses/gpl-3.0.html">GPL-3.0</a></td>
        <td><a href="../theme/js/navbar-burger.js">navbar-burger.js</a></td>
    </tr>
</table>
