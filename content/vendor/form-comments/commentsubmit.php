<?php

$post_id = $_POST["post_id"];
$return_url = $_POST["return_url"];

// Slug
function seourl($string) {
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}

// Check for empty fields
if(empty($_POST['name'])    ||
   empty($_POST['comment']) ||
   empty($_POST['email'])   ||
   !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)
)
{
    // Rediret to current post
    header( "Location: {$return_url}");
} else {

    $DATE_FORMAT = "Y-m-d H:i:s";
    $publish = date($DATE_FORMAT);

    $name = strip_tags(utf8_decode(utf8_encode($_POST['name'])));
    $link = strip_tags(htmlspecialchars($_POST['link']));
    $email_address = strip_tags(htmlspecialchars($_POST['email']));
    $comment = utf8_decode($_POST['comment']);

    // article
    $postID = str_replace('/','',$post_id);

    // web-site
    if (!empty($link)) {
        $web = "Web: {$link}";
    } else {
        $web = NULL;
    }

    //slug
    $nslug = seourl($name);
    $fslug = date("Ymd-H:i:s");
    $slug = "$nslug-$fslug";

    // Create the email and send the message
    // Add your email address
    $local_address = "heckyel@riseup.net";
    $recipients = array(
        $local_address,
        $email_address,
        // more emails
    );
    $to = implode(',', $recipients);
    $email_subject = <<<EOT
[conocimientoslibres.tuxfamily.org] Mensaje de {$name}
EOT;

    $email_body = <<<EOT
Nuevo comentario del formulario de conocimientoslibres.tuxfamily.org
Aqui estan los detalles:\n
post_id: {$postID}
Author: {$name}
Date: {$publish}
Email: {$email_address}
Slug: {$slug}
{$web}\n
{$comment}\n\n
¿Usted no ha escrito este comentario?
Responde este mensaje para eliminar el comentario.
EOT;

    $headers = "From: noreply@conocimientoslibres.tuxfamily.org\n"; // Using something like noreply@yourdomain.com.
    $headers .= "Reply-To: $local_address";
    mail($to,$email_subject,utf8_decode($email_body),$headers);

    // Rediret to current post
    header("Refresh: 10; URL={$return_url}");
    printf('Hurra! %s su comentario se envió correctamente,
volviendo a la web en 10 segundos...', $name);
}
