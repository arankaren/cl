function init(){
    let video = document.getElementById('videoplaylist');
    let playlist = document.getElementById('playlist');
    let tracks = playlist.getElementsByTagName('a');
    video.volume = 0.50;

    //Cuenta los tracks
    for(let track in tracks) {
        let link = tracks[track];
        if(typeof link === "function" || typeof link === "number"){continue;}
        link.addEventListener('click', function(e) {
            e.preventDefault();
            let song = this.getAttribute('href');
            run(song, video, this);
        });
    }
    //Agregamos evento para reproducir el siguiente items
    video.addEventListener('ended',function(e) {
        for(let track in tracks) {
            let link = tracks[track];
            let nextTrack = parseInt(track) + 1;
            if(typeof link === "function" || typeof link === "number"){continue;}
            if(!this.src){this.src = tracks[0];}
            if(track == (tracks.length - 1)){nextTrack = 0;}
            console.log(nextTrack);
            if(link.getAttribute('href') === this.src) {
                let nextLink = tracks[nextTrack];
                run(nextLink.getAttribute('href'), video, nextLink);
                break;
            }
        }
    });
}

function run(song, video, link){
    let parent = link.parentElement;
    //Quita el active de todos los elementos de la lista
    let items = parent.parentElement.getElementsByTagName('li');
    for(let item in items) {
        if (items[item].classList) {
            items[item].classList.remove("is-active-play");
        }
    }
    //Agrega active a este elemento
    parent.classList.add("is-active-play");
    //Inicia la reproducción
    video.src = song;
    video.load();
    video.play();
}

init();
