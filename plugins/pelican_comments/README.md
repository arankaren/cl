# pelican-comments

This is a plugin for [pelican](http://getpelican.com) to provide static comments.

It was inspired by a [similar plugin for
Jekyll](https://github.com/mpalmer/jekyll-static-comments).

# How It Works

When enabled, the plugin searches a comments directory in your pelican
tree. In that directory, there is one file per comment. Each comment
file has a `post_id` attribute with the slug of the post to which the
comment belongs. The article template will receive a `comments` attribute
which contains the list of comments on the post.

# Installation

Pelican-comments is not part of the default pelican distribution.

In your settings.py, add:

    PLUGINS = ['pelican_comments']
    COMMENTS_DIR = ['comments'] # Optional: 'comments' is the default
    ARTICLE_EXCLUDES = ['comments', 'pages'] # Optional: Stops pelican trying to validate comments as articles

In your template, add:

    {% if article.comments %}
        <h2>Comments</h2>
        <ul>
          {% for comment in article.comments %}
            <li>
              <p>{{ comment.author }} said, on {{ comment.date }}:</p>
              {{ comment.content }}
            </li>
          {% endfor %}
        </ul>
    {% endif %}

In your pelican tree, create a comments directory. Add one file per
comment on your blog. Make sure that each comment file has a `post_id`
attribute to tie it to a post. You can also include the comments'
authors and date/time. For example:

    post_id: one-of-my-post-slugs
    Author: some random guy
    Date: 2019-09-27 18:44

    This is a test.

It doesn't matter what the files are named.

# How to mention a user ?

Because there is not JavaScript in the comment system.
Mentioning a user can be something difficult, however
we just have to place a link at the beginning of the
comment, here I show an example:

    :::bash
    [@snowden](#comment-snowden-20121118-20:14:50)
    Reply message.

where:

- `#comment-snowden-20121118-20:14:50` is the comment `id` of the
  user to respond.

# Example folder structure

	.
	└── comments
		└── foo-bar
		│   ├── 1.md
		│   └── 0.md
		└── some-other-slug
			├── random-Name.md
			├── 1.md
			└── 0.md

# Generating the Comment Files

Script for processing a comment submission form coming soon.

Script to import comments from a wordpress export file coming soon.

See also
[this php
script](https://github.com/mpalmer/jekyll-static-comments/blob/master/commentsubmit.php),
which should be relatively easy to hack into
submission. More discussion [in this blog
post](http://hezmatt.org/~mpalmer/blog/2011/07/19/static-comments-in-jekyll.html).

You'll need some kind of workflow to allow you to move comments from
email or other holding area (I plan to use the filesystem on the webhost)
to your pelican tree.
