# -*- coding: utf-8 -*-
"""
css-js-minify wrapper for Pelican
"""

import glob

from .minify import (
    process_single_css_file,
    process_single_js_file,
)

from pelican import signals


def main(pelican):
    """ Compiler """
    for file in glob.iglob(pelican.output_path + '/**/*.css', recursive=True):
        process_single_css_file(file, overwrite=True)
    for file in glob.iglob(pelican.output_path + '/**/*.js', recursive=True):
        process_single_js_file(file, overwrite=True)


def register():
    """ Register """
    signals.finalized.connect(main)


SUPPORT_JS = """
-----------------------------------------------------------------
COMPRESSOR:
-----------------------------------------------------------------
Future JavaScript support is orphan and not supported!
If you want to make ES6,ES7 work feel free to send pull requests.
-----------------------------------------------------------------
"""
print(SUPPORT_JS)
