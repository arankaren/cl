#!/bin/bash

# Load data
printf '%s' "Enter Title: "
read -r
title="$REPLY"

printf '%s' "Enter Author: "
read -r
author="$REPLY"

printf '%s' "Enter Category: "
read -r
category="$REPLY"

# Slug | lowercase, without accent mark
lug=$(printf '%s' "${title// /-}")
slug=$(echo "$lug" | sed 'y/áÁàÀãÃâÂéÉêÊíÍóÓõÕôÔúÚñÑçÇ/aAaAaAaAeEeEiIoOoOoOuUnNcC/' | sed -e 's/\(.*\)/\L\1/')

printf '%s' "Enter Tags: "
read -r
tag="$REPLY"

# output
o_author=$(printf '%s%s\n' "Author: " "$author")
o_category=$(printf '%s%s\n' "Category: " "$category")
o_date=$(printf '%s%s\n' "Date: " "$(date +"%Y-%m-%d %I:%M")")
o_slug=$(printf '%s%s\n' "Slug: " "$slug")
o_tag=$(printf '%s%s\n' "Tags: " "$tag")
o_title=$(printf '%s%s\n' "Title: " "$title")

# export to file.md
printf '%s\n%s\n%s\n%s\n%s\n%s\n' "$o_author"\
                                  "$o_category"\
                                  "$o_date"\
                                  "$o_slug"\
                                  "$o_tag"\
                                  "$o_title" > "content/$slug.md"
