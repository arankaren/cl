# -*- coding: utf-8 -*- #

from __future__ import unicode_literals

# from pelican import __version__ as PELICAN_VERSION

# Basic settings
# https://docs.getpelican.com/en/stable/settings.html#basic-settings
DEFAULT_CATEGORY = 'Sin categoría'
DELETE_OUTPUT_DIRECTORY = False
DISPLAY_CATEGORIES_ON_MENU = True
DISPLAY_PAGES_ON_MENU = True
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.toc': {
            'title': 'Tabla de Contenidos',
            'permalink': 'true'
        },
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.footnotes': {
            'BACKLINK_TITLE': 'Volver a la nota %d en el texto'
        },
        'markdown.extensions.meta': {},
    },
    'output_format': 'html5',
}
# Fix issue with < and > characters (among others) being double-escaped
# Bug report in markdown/extensions/codehilite.py
# https://github.com/Python-Markdown/markdown/pull/726/files

PATH = 'content'
PLUGIN_PATHS = ['plugins']
PLUGINS = [
    'extract_toc',
    'i18n_subsites',
    'neighbors',
    'pelican-css',
    'pelican-js',
    'sitemap',
    'tag-cloud',
    'tipue-search',
    'pelican_comments'
]

# Optional: 'comments' is the default
COMMENTS_DIR = ['comments']

SITENAME = 'Conocimientos Libres'
SITENAME_SINGLE = 'CL'
SITEURL = 'https://conocimientoslibres.tuxfamily.org'
AUTHOR = 'Jesús E.'

# URL settings
# https://docs.getpelican.com/en/stable/settings.html#url-settings
RELATIVE_URLS = True
ARTICLE_URL = '{slug}/'
ARTICLE_SAVE_AS = '{slug}/index.html'
CATEGORY_URL = 'category/{slug}/'
CATEGORY_SAVE_AS = 'category/{slug}/index.html'
CATEGORIES_URL = 'categories/'
CATEGORIES_SAVE_AS = 'categories/index.html'
TAG_URL = 'tag/{slug}/'
TAG_SAVE_AS = 'tag/{slug}/index.html'
TAGS_URL = 'tags/'
TAGS_SAVE_AS = 'tags/index.html'
AUTHOR_URL = 'author/{slug}/'
AUTHOR_SAVE_AS = 'author/{slug}/index.html'
YEAR_ARCHIVE_SAVE_AS = 'archives/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'archives/{date:%Y}/{date:%m}/index.html'
AUTHORS_URL = 'pages/créditos/'
AUTHORS_SAVE_AS = 'pages/créditos/index.html'
ARCHIVES_URL = 'archives/'
ARCHIVES_SAVE_AS = 'archives/index.html'
STATIC_PATHS = [
    '.htaccess',
    'heckyel_pub.asc',
    'robots.txt',
    'wp-content',
    'vendor'
]

PAGE_PATHS = ['pages']
PAGE_EXCLUDES = [
    'vendor',
    'wp-content'
]

ARTICLE_PATHS = ['articles']
ARTICLE_EXCLUDES = [
    'comments',
    'pages',
    'vendor',
    'wp-content'
]

# Time and date
# https://docs.getpelican.com/en/stable/settings.html#time-and-date
TIMEZONE = 'Europe/Madrid'
LOCALE = ('es_ES.UTF-8')

# Feed settings
# https://docs.getpelican.com/en/stable/settings.html#feed-settings
# feed generation is usually not desired when developing,
# set to true in publishconf.py
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Pagination (https://docs.getpelican.com/en/stable/settings.html#pagination)
DEFAULT_PAGINATION = 3
PAGINATION_PATTERNS = (
    (1, '{base_name}/', '{base_name}/index.html'),
    (2, '{base_name}/page/{number}/', '{base_name}/page/{number}/index.html'),
)
PAGINATOR_LIMIT = 4

# Translations
# https://docs.getpelican.com/en/stable/settings.html#translations
DEFAULT_LANG = 'es'
TRANSLATION_FEED_ATOM = None

# Themes
# https://docs.getpelican.com/en/stable/settings.html#themes
THEME = 'cl-theme/'
SHOW_RECENT_POSTS = 3  # the number of recent posts to show

# Plugins' configuration (not from Pelican core)
TAG_CLOUD_STEPS = 5
TAG_CLOUD_MAX_ITEMS = 53
TAG_CLOUD_SORTING = 'random'
TAG_CLOUD_BADGE = True

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'yearly',
        'indexes': 'weekly',
        'pages': 'yearly'
    }
}

SOCIAL = {
    'diaspora': '#',
    'gnusocial': '#',
    'mastodom': '#',
    'matrix': '#',
    'mediagoblin': '#',
    'peertube': '#',
    'pixelfed': '#',
    'pump': '#'
}

I18N_UNTRANSLATED_ARTICLES = 'remove'
I18N_UNTRANSLATED_PAGES = 'remove'
I18N_SUBSITES = {
    'de': {
        'LOCALE': ('de_DE.UTF-8'),
    },
    'en': {
        'LOCALE': ('en_US.UTF-8'),
        'MARKDOWN': {
            'extension_configs': {
                'markdown.extensions.toc': {
                    'title': 'Table of Contents',
                },
                'markdown.extensions.codehilite': {'css_class': 'highlight'},
                'markdown.extensions.extra': {},
                'markdown.extensions.footnotes': {
                    'BACKLINK_TITLE': 'Jump back to footnote %d in the text'
                },
                'markdown.extensions.meta': {},
            },
            'output_format': 'html5',
        }
    },
    'eo': {
        'LOCALE': ('eo.UTF-8'),
    },
    'fr': {
        'LOCALE': ('fr_FR.UTF-8'),
    },
}
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
